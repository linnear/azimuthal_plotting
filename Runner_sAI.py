# -*- coding: utf-8 -*-
"""
Created on Tue Jan  9 13:44:14 2024

@author: rensmo_l
"""

from sAI import SASazimuthal
import fabio
import numpy as np
import matplotlib.pyplot as plt

#sample = 'PMMA'
#sample = 'conc1_4'
#sample = 'conc2_0'
#sample = 'conc4_0'
sample = 'conc5_0'

cbf_file_path = f"C:\\Users\\rensmo_l\\Documents\\Azim_int\{sample}.cbf"
obj = fabio.open(cbf_file_path)
custom_vmin = -10
custom_vmax = 4
log_offset = 1e-10  # Small constant to avoid log(0)

data = obj.data + log_offset  # Add a small constant to avoid log(0)
log_data = np.log10(data) # Apply logarithmic transformation

# Couldn't figure out how to make it centered without first creating a symmetric matrix from the data
center_index = (985,710)
# Extract the quadratic matrix centered around the specified index
size = 1400
half_size = size // 2
centered_data = data[center_index[0] - half_size:center_index[0] + half_size,
                             center_index[1] - half_size:center_index[1] + half_size]

# Run sAI
sai_object = SASazimuthal(centered_data)
sai_object.showresults()